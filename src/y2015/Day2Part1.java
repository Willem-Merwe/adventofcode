package y2015;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class Day2Part1 {

	public static void main(String[] args) {
		String file_name = "C:/day2.txt";
		
		int l = 0;
		int w = 0;
		int h = 0;
		
		int lw = 0;
		int lh = 0;
		int wh = 0;
		
		int extra = 0;
		
		int total = 0;
		
		try{
			FileReader fr = new FileReader(file_name);
			Scanner sc = new Scanner(fr);
			String fullLine;
			String[] parts;
			sc.useDelimiter("x");
			
			while(sc.hasNextLine()){
				fullLine = sc.nextLine();
				parts = fullLine.split("x");
				
				l = Integer.parseInt(parts[0]);
				w = Integer.parseInt(parts[1]);
				h = Integer.parseInt(parts[2]);

				lw = l*w;
				lh = l*h;
				wh = w*h;
				
				extra = lw;
				if(extra > lh)
					extra = lh;
				if(extra > wh)
					extra = wh;
				
				total += (2*lw) + (2*wh) + (2*lh) + extra;
			}
			
			System.out.println(total);
		} catch (IOException e) {
			System.out.println("Didn't Work");
		}
	}

}
