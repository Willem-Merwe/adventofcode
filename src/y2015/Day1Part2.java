package y2015;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class Day1Part2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String file_name = "C:/day1.txt";
		
		try{
			FileReader fr = new FileReader(file_name);
			BufferedReader br = new BufferedReader(fr);
			
			int floor = 0;
			int ans = 0;
			int counter = 0;
			char testingPart = ' ';
			
			boolean test = true;
			
			while(test){
				testingPart = (char)br.read();
				
				if(testingPart =='(')
					floor++;
				else if(testingPart == ')')
					floor--;
				else {
					test = false;
				}
				
				if(floor == -1)
					test=false;
				counter++;
			}
			
			System.out.println(counter);
			
		} catch(IOException e){
			System.out.println("didnt work");
		}
	}
}