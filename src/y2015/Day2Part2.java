package y2015;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Scanner;



public class Day2Part2 {

	public static void main(String[] args) {
		String file_name = "C:/day2.txt";
		
		int l = 0;
		int w = 0;
		int h = 0;
		
		int lw = 0;
		int lh = 0;
		int wh = 0;
		
		int oneSide = 0;
		int secondSide = 0;
		
		int total = 0;
		
		try{
			FileReader fr = new FileReader(file_name);
			Scanner sc = new Scanner(fr);
			String fullLine;
			String[] parts;
			int[] num = new int[3];
			sc.useDelimiter("x");
			
			while(sc.hasNextLine()){
				fullLine = sc.nextLine();
				parts = fullLine.split("x");
				
				num[0] = Integer.parseInt(parts[0]);
				num[1] = Integer.parseInt(parts[1]);
				num[2] = Integer.parseInt(parts[2]);
				
				Arrays.sort(num);
				
				total += (2*num[0]) + (2*num[1]) + (num[0] * num[1] * num[2]);
			}
			
			System.out.println(total);
		} catch (IOException e) {
			System.out.println("Didn't Work");
		}
	}

}
